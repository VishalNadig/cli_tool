import uvicorn
import configparser
from fastapi import FastAPI, HTTPException
from fastapi.responses import FileResponse
import main

config = configparser.ConfigParser()
file = r"/Users/akshathanadig/Downloads/Work/Demo/cli_tool/config.ini"
config.read(file)
file_path = "sql_query_result.csv"

app = FastAPI()


@app.get("/run_sql")
def run_sql_query(env: str, command: str):
    value = main.run_sql(env=env, command=command)
    if value[0] is None:
        raise HTTPException(status_code=404, detail=str(value[1]))
    return {"status": 200, "detail": value}


@app.get("/download_csv")
def download():
    return FileResponse(
        file_path, status_code=200, media_type="text/csv", filename="result.csv"
    )


# def get_details(env):
#     print(config[env]["HOSTNAME"])


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=7879)


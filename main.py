import pymysql
import logging
import configparser
import pandas as pd

config = configparser.ConfigParser()
file = "config.ini"
config.read(file)

def get_connection(env: str):
    env = env.lower()
    HOSTNAME = config[env]['HOSTNAME']
    USER = config[env]['USER']
    PASSWORD = config[env]['PASSWORD']
    PORT = int(config[env]['PORT'])
    connection = pymysql.connect(host= HOSTNAME, user= USER, password= PASSWORD, port= PORT)
    return connection

def run_sql(env: str, command: str):
    try:
        connection = get_connection(env=env)
        cursor = connection.cursor()
        cursor.execute(command)
        result = pd.read_sql(command, connection)
        result.to_csv("sql_query_result.csv", index= False)
        return cursor.fetchall()

    except Exception as exception:
        return(None, exception)

    # finally:
    #     if connection.open:
    #         cursor.close()
    #         connection.close()

if __name__ is '__main__':
    pass

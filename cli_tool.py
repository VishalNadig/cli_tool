import argparse
import main

def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--comand", type= str, help= "Enter the command name", required= True)
    parser.add_argument("-e", "--env", type= str, help= "Enter the connecting environment", required= True)
    args = parser.parse_args()
    return args

args = get_parser()

def run_query(env, command):
    main.run_sql(env, command)

run_query(env = args.env, command= args.command)

